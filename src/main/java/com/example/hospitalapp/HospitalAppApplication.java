package com.example.hospitalapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@SpringBootApplication
public class HospitalAppApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(HospitalAppApplication.class, args);
	}



	@Override
	public void run(String... args) {

		System.out.println("Sending Email...");

		//sendEmail();
		//sendEmailWithAttachment();

		System.out.println("Done");

	}

}
