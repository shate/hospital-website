package com.example.hospitalapp.persistance.repository;

import com.example.hospitalapp.persistance.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {
}
