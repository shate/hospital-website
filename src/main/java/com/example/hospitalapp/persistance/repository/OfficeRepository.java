package com.example.hospitalapp.persistance.repository;

import com.example.hospitalapp.persistance.entity.Office;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficeRepository extends JpaRepository<Office, Long> {
}
