package com.example.hospitalapp.persistance.entity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "doctors")
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String surname;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;
    private String username;
    private String password;
    private String email;
    private int experience;
    private float rating;
    @ManyToMany
    private List<Office> offices;
    private String education;
    private String description;
    @ManyToMany
    private List<Treatment> treatments;
    @OneToMany
    private List<DaySchedule> schedules;

    public Doctor() {
    }

    public Doctor(String firstName, String surname, LocalDate dateOfBirth, String username, String password, String email,
                  int experience, float rating, List<Office> offices, String education,
                  String description, List<Treatment> treatments, List<DaySchedule> schedules) {
        this.firstName = firstName;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.username = username;
        this.password = password;
        this.email = email;
        this.experience = experience;
        this.rating = rating;
        this.offices = offices;
        this.education = education;
        this.description = description;
        this.treatments = treatments;
        this.schedules = schedules;
    }

    public Doctor(Long id, String firstName, String surname, LocalDate dateOfBirth, String username, String password,
                  String email, int experience, float rating, List<Office> offices,
                  String education, String description, List<Treatment> treatments, List<DaySchedule> schedules) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.username = username;
        this.password = password;
        this.email = email;
        this.experience = experience;
        this.rating = rating;
        this.offices = offices;
        this.education = education;
        this.description = description;
        this.treatments = treatments;
        this.schedules = schedules;
    }

    public List<DaySchedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<DaySchedule> schedules) {
        this.schedules = schedules;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public List<Office> getOffices() {
        return offices;
    }

    public void setOffices(List<Office> offices) {
        this.offices = offices;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Treatment> getTreatments() {
        return treatments;
    }

    public void setTreatments(List<Treatment> treatments) {
        this.treatments = treatments;
    }



    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", experience=" + experience +
                ", rating=" + rating +
                ", offices=" + offices +
                ", education='" + education + '\'' +
                ", description='" + description + '\'' +
                ", treatments=" + treatments +
                ", schedules=" + schedules +
                '}';
    }
}