package com.example.hospitalapp.persistance.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
public class Visit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    @OneToOne
    private Meeting meeting;
    @OneToOne
    private Doctor doctor;
    @OneToOne
    private Treatment treatment;
    @OneToOne
    private Customer customer;
    @ManyToOne
    private MedicalHistory medicalHistory;

    public Visit() {
    }


    public MedicalHistory getMedicalHistory() {
        return medicalHistory;
    }

    public void setMedicalHistory(MedicalHistory medicalHistory) {
        this.medicalHistory = medicalHistory;
    }

    public Visit(Meeting meeting, LocalDate date, Doctor doctor, Treatment treatment, Customer customer, MedicalHistory medicalHistory) {
        this.meeting = meeting;
        this.date = date;
        this.doctor = doctor;
        this.treatment = treatment;
        this.customer = customer;
        this.medicalHistory = medicalHistory;
    }

    public Visit(Long id, Meeting meeting, LocalDate date, Doctor doctor, Treatment treatment, Customer customer) {
        this.meeting = meeting;
        this.id = id;
        this.date = date;
        this.doctor = doctor;
        this.treatment = treatment;
        this.customer = customer;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Meeting getMeeting() {
        return meeting;
    }

    public void setMeeting(Meeting meeting) {
        this.meeting = meeting;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Treatment getTreatment() {
        return treatment;
    }

    public void setTreatment(Treatment treatment) {
        this.treatment = treatment;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Visit{" +
                "id=" + id +
                ", date=" + date +
                ", meeting=" + meeting +
                ", doctor=" + doctor +
                ", treatment=" + treatment +
                ", customer=" + customer +
                ", medicalHistory=" + medicalHistory +
                '}';
    }
}
