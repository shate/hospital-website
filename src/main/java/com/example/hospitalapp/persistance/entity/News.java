package com.example.hospitalapp.persistance.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany
    private List<Article> articles;

    public News() {
    }

    public News(List<Article> articles) {
        this.articles = articles;
    }

    public News(Long id, List<Article> articles) {
        this.id = id;
        this.articles = articles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
