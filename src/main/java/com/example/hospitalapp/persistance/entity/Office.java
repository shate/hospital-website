package com.example.hospitalapp.persistance.entity;

import javax.persistence.*;
import javax.print.Doc;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "offices")
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String type;
    private String description;
    @OneToMany
    private List<Treatment> treatments;



    public Office() {
    }

    public Office(String type, String description, List<Treatment> treatments) {
        this.type = type;
        this.description = description;
        this.treatments = treatments;
    }

    public Office(Long id, String type, String description) {
        this.id = id;
        this.type = type;
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Treatment> getTreatments() {
        return treatments;
    }

    public void setTreatments(List<Treatment> treatments) {
        this.treatments = treatments;
    }

    @Override
    public String toString() {
        return "Office{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", treatments=" + treatments +
                '}';
    }
}
