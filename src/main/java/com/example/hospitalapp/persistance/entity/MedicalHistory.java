package com.example.hospitalapp.persistance.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "medical_histories")
public class MedicalHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany()
    List<Visit> visits;
    @ManyToMany
    List<Medicine> medicines;
    @ManyToMany
    List<Disease> diseases;


    public MedicalHistory() {
    }

    public MedicalHistory(List<Visit> visits, List<Medicine> medicines, List<Disease> diseases) {
        this.visits = visits;
        this.medicines = medicines;
        this.diseases = diseases;
    }

    public MedicalHistory(Long id, List<Visit> visits, List<Medicine> medicines, List<Disease> diseases) {
        this.id = id;
        this.visits = visits;
        this.medicines = medicines;
        this.diseases = diseases;
    }

    public List<Medicine> getMedicines() {
        return medicines;
    }

    public void setMedicines(List<Medicine> medicines) {
        this.medicines = medicines;
    }

    public List<Disease> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<Disease> diseases) {
        this.diseases = diseases;
    }

    public List<Visit> getVisits() {
        return visits;
    }

    public void setVisits(List<Visit> visits) {
        this.visits = visits;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "MedicalHistory{" +
                "id=" + id +
                '}';
    }
}
