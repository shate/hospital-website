package com.example.hospitalapp.persistance.entity;

import javax.persistence.*;

@Entity
@Table(name = "diseases")
public class Disease {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String type;
    private String basicInfo;

    public Disease() {
    }



    public Disease(String type, String basicInfo) {
        this.type = type;
        this.basicInfo = basicInfo;
    }

    public Disease(Long id, String type, String basicInfo) {
        this.id = id;
        this.type = type;
        this.basicInfo = basicInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBasicInfo() {
        return basicInfo;
    }

    public void setBasicInfo(String basicInfo) {
        this.basicInfo = basicInfo;
    }
}
