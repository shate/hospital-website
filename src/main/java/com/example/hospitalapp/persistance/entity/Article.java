package com.example.hospitalapp.persistance.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String content;
    @ManyToMany
    private List<Office> categories;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate publishDate;
    private String author;

    public Article() {
    }

    public Article(String title, String content, List<Office> categories, LocalDate publishDate, String author) {
        this.title = title;
        this.content = content;
        this.categories = categories;
        this.publishDate = publishDate;
        this.author = author;
    }

    public Article(Long id, String title, String content, List<Office> categories, LocalDate publishDate, String author) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.categories = categories;
        this.publishDate = publishDate;
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Office> getCategories() {
        return categories;
    }

    public void setCategories(List<Office> categories) {
        this.categories = categories;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
