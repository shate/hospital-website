package com.example.hospitalapp.persistance.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "daySchedules")
public class DaySchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    @OneToMany
    List<Meeting> meetings;

    public DaySchedule() {
    }

    public DaySchedule(LocalDate date, List<Meeting> meetings) {
        this.date = date;
        this.meetings = meetings;
    }

    public DaySchedule(Long id, LocalDate date, List<Meeting> meetings) {
        this.id = id;
        this.date = date;
        this.meetings = meetings;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DaySchedule{" +
                "id=" + id +
                ", date=" + date +
                ", meetings=" + meetings +
                '}';
    }

    public List<Meeting> getMeetings() {
        return meetings;
    }

    public void setMeetings(List<Meeting> meetings) {
        this.meetings = meetings;
    }
}
