package com.example.hospitalapp.controller;

import com.example.hospitalapp.persistance.entity.Article;
import com.example.hospitalapp.persistance.entity.News;
import com.example.hospitalapp.service.ArticleService;
import com.example.hospitalapp.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class NewsController {

    private ArticleService articleService;
    private NewsService newsService;

    @Autowired
    public NewsController(ArticleService articleService, NewsService newsService) {
        this.articleService = articleService;
        this.newsService = newsService;
    }

    @GetMapping("/news")
    public String getNews(Model model){
        News news = newsService.findById(1L).get();
        List<Article> articles = news.getArticles();
        model.addAttribute("articles", articles);
        return "news";
    }
}
