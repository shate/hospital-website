package com.example.hospitalapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AboutUsController {


    @GetMapping("/about")
    public String aboutUs(Model model){

        return "about";
    }
}
