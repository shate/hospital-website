package com.example.hospitalapp.controller;

import com.example.hospitalapp.persistance.entity.Doctor;
import com.example.hospitalapp.persistance.entity.Office;
import com.example.hospitalapp.persistance.entity.Treatment;
import com.example.hospitalapp.service.DoctorService;
import com.example.hospitalapp.service.OfficeService;
import com.example.hospitalapp.service.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Controller
public class TreatmentController {
    private TreatmentService treatmentService;
    private OfficeService officeService;
    private DoctorService doctorService;

    @Autowired
    public TreatmentController(DoctorService doctorService, OfficeService officeService, TreatmentService treatmentService){
        this.treatmentService = treatmentService;
        this.officeService = officeService;
        this.doctorService = doctorService;
    }


    @GetMapping("/treatments")
    public String getAllTreatments(Model model){
        List<Office> offices = officeService.findAll();
        model.addAttribute("offices", offices);
        return "treatments";
    }


    @GetMapping("/treatments/{id}")
    public String getDoctor(@PathVariable("id") Long treatmentID, Model model){
        Treatment treatment = treatmentService.findById(treatmentID).get();
        List<Doctor> doctors = doctorService.findAll();
        model.addAttribute("doctors", doctors);
        model.addAttribute("treatment", treatment);
        return "treatment";
    }
}
