package com.example.hospitalapp.controller;

import com.example.hospitalapp.persistance.entity.Office;
import com.example.hospitalapp.service.DoctorService;
import com.example.hospitalapp.service.OfficeService;
import com.example.hospitalapp.service.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
@Controller
public class PriceController {

    private TreatmentService treatmentService;
    private DoctorService doctorService;
    private OfficeService officeService;

    @Autowired
    public PriceController(TreatmentService treatmentService, DoctorService doctorService, OfficeService officeService) {
        this.treatmentService = treatmentService;
        this.doctorService = doctorService;
        this.officeService = officeService;
    }

    @GetMapping("/prices")
    public String getPrices(Model model){
        List<Office> offices = officeService.findAll();
        model.addAttribute("offices", offices);
        return "prices";
    }
}
