package com.example.hospitalapp.controller;

import com.example.hospitalapp.persistance.entity.Doctor;
import com.example.hospitalapp.service.DayScheduleService;
import com.example.hospitalapp.service.DoctorService;
import com.example.hospitalapp.service.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@Controller
public class DoctorController {
    private DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService){
        this.doctorService = doctorService;
    }

    @GetMapping("/doctors")
    public String getDoctors(Model model){
        List<Doctor> doctors = doctorService.findAll();
        model.addAttribute("doctors", doctors);
        return "doctors";
    }

    @GetMapping("/doctors/{id}")
    public String getDoctor(@PathVariable("id") Long doctorID, Model model){
        Doctor doctor = doctorService.finByID(doctorID).get();
        model.addAttribute("doctor", doctor);
        return "doctor";
    }
}
