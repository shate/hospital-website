package com.example.hospitalapp.controller;

import com.example.hospitalapp.persistance.entity.Customer;
import com.example.hospitalapp.persistance.entity.MedicalHistory;
import com.example.hospitalapp.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/accountInfo")
    public String accountInfo(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Customer customer = customerService.findByUsername(auth.getName());
        model.addAttribute("customer", customer);
        return "account_info";
    }


    @GetMapping("/medicalHistory")
    public String medicalHistory(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Customer customer = customerService.findByUsername(auth.getName());
        MedicalHistory medicalHistory = customer.getMedicalHistory();
        model.addAttribute("medicalHistory", medicalHistory);
        return "medical_history";
    }
}
