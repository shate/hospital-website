package com.example.hospitalapp.controller;

import com.example.hospitalapp.persistance.entity.*;
import com.example.hospitalapp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller
public class VisitController {
    private VisitService visitService;
    private OfficeService officeService;
    private DoctorService doctorService;
    private TreatmentService treatmentService;
    private DayScheduleService dayScheduleService;
    private MeetingService meetingService;
    private CustomerService customerService;
    private UserService userService;
    private MedicalHistoryService medicalHistoryService;

    @Autowired
    public VisitController(MedicalHistoryService medicalHistoryService, UserService userService, CustomerService customerService, DayScheduleService dayScheduleService, MeetingService meetingService, TreatmentService treatmentService, OfficeService officeService, VisitService visitService, DoctorService doctorService){
        this.medicalHistoryService = medicalHistoryService;
        this.userService = userService;
        this.customerService =customerService;
        this.treatmentService = treatmentService;
        this.officeService = officeService;
        this.visitService = visitService;
        this.doctorService = doctorService;
        this.dayScheduleService = dayScheduleService;
        this.meetingService = meetingService;
    }

    @GetMapping("/bookVisit")
    public String getAllOffices(Model model, HttpSession session){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Customer customer = customerService.findByUsername(auth.getName());
        List<Office> offices = officeService.findAll();
        session.setAttribute("customer", customer);
        session.setAttribute("office", null);
        session.setAttribute("treatment", null);
        session.setAttribute("doctor", null);
        session.setAttribute("date", null);
        session.setAttribute("meeting", null);
        model.addAttribute("offices", offices);
        return "book_new_visit";
    }


    @GetMapping("/treatment")
    public String backToTreatments(Model model, HttpSession session){
        Office office = (Office) session.getAttribute("office");
        session.setAttribute("treatment", null);
        session.setAttribute("doctor", null);
        session.setAttribute("date", null);
        session.setAttribute("meeting", null);
        List<Treatment> treatments = office.getTreatments();
        model.addAttribute("treatments", treatments);
        return "book_new_visit";
    }


    @PostMapping("/treatment")
    public String getTreatmentsForOffice(@RequestParam("officeID") Long officeID, Model model, HttpSession session){
        Office office = officeService.findById(officeID).get();
        List<Treatment> treatments = office.getTreatments();
        session.setAttribute("office", office);
        session.setAttribute("treatment", null);
        session.setAttribute("doctor", null);
        session.setAttribute("date", null);
        session.setAttribute("meeting", null);
        model.addAttribute("treatments", treatments);
        return "book_new_visit";
    }

    @GetMapping("/doctor")
    public String backToDoctors(Model model, HttpSession session){
        Treatment treatment = (Treatment) session.getAttribute("treatment");
        List<Doctor> doctors = doctorService.getForTreatment(treatment.getId());
        session.setAttribute("treatment", treatment);
        session.setAttribute("doctor", null);
        session.setAttribute("date", null);
        session.setAttribute("meeting", null);
        model.addAttribute("doctors", doctors);
        return "book_new_visit";
    }

    @PostMapping("/doctor")
    public String getDoctorsForOfficeAndTreatment(@RequestParam Long treatmentID, Model model, HttpSession session){
        Treatment treatment = treatmentService.findById(treatmentID).get();
        System.out.println(treatment.toString());
        List<Doctor> doctors = doctorService.getForTreatment(treatment.getId());
        System.out.println(doctors.toString());
        session.setAttribute("treatment", treatment);
        session.setAttribute("doctor", null);
        session.setAttribute("date", null);
        session.setAttribute("meeting", null);
        model.addAttribute("doctors", doctors);
        return "book_new_visit";
    }


    @GetMapping("/time")
    public String backTimes(Model model, HttpSession session){
        Doctor doctor = (Doctor) session.getAttribute("doctor");
        List<DaySchedule> dates = doctor.getSchedules();
        session.setAttribute("doctor", doctor);
        session.setAttribute("date", null);
        session.setAttribute("meeting", null);
        model.addAttribute("dates", dates);
        return "book_new_visit";
    }


    @PostMapping("/time")
    public String getTimes(@RequestParam Long doctorID, Model model, HttpSession session){
        Doctor doctor = doctorService.finByID(doctorID).get();
        List<DaySchedule> dates = doctor.getSchedules();
        session.setAttribute("doctor", doctor);
        session.setAttribute("date", null);
        session.setAttribute("meeting", null);
        model.addAttribute("dates", dates);
        return "book_new_visit";
    }

    @PostMapping("/book")
    public String book(@RequestParam(name = "dateID") Long dateID, @RequestParam(name = "meetingID") Long meetingID, Model model, HttpSession session){
        DaySchedule date = dayScheduleService.findByID(dateID).get();
        Meeting meeting = meetingService.findById(meetingID).get();
        session.setAttribute("date", date);
        session.setAttribute("meeting", meeting);
        model.addAttribute("confirm", true);
        return "book_new_visit";
    }

    @PostMapping("/confirm")
    public String confirm(HttpSession session, Model model){
        DaySchedule daySchedule = (DaySchedule) session.getAttribute("date");
        LocalDate date = daySchedule.getDate();
        Meeting meeting = (Meeting) session.getAttribute("meeting");
        meetingService.findById(meeting.getId()).get().setAvailable(false);
        LocalTime startTime = meeting.getStartTime();
        LocalTime endTime = meeting.getEndTime();
        Doctor doctor = (Doctor) session.getAttribute("doctor");
        Treatment treatment = (Treatment) session.getAttribute("treatment");
        Customer customer = (Customer) session.getAttribute("customer");
        MedicalHistory medicalHistory = medicalHistoryService.findById(customer.getMedicalHistory().getId()).get();

        Visit visit = new Visit(meeting, date, doctor, treatment, customer, medicalHistory);
        visitService.save(visit);
        medicalHistory.getVisits().add(visit);
        medicalHistoryService.save(medicalHistory);
        List<Visit> visits = customerService.getVisitsById((customer.getId()));
        List<Visit> newVisits = new ArrayList<>();
        if(!visits.isEmpty()) {
            visits.forEach(visit1 -> {
                if (!visit1.getDate().isBefore(LocalDate.now())) {
                    newVisits.add(visit1);
                }
            });
        }
        model.addAttribute("medicalHistory", medicalHistory);
        model.addAttribute("visits", newVisits);
        return "booked_visits";
    }

    @GetMapping("/visits")
    public String getAllVisits(Model model){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(auth.getName());

        Customer customer = customerService.findByUsername(user.getUsername());
        List<Visit> visits = customerService.getVisitsById((customer.getId()));
        MedicalHistory medicalHistory = customer.getMedicalHistory();
        List<Visit> newVisits = new ArrayList<>();
        if(!visits.isEmpty()) {
            visits.forEach(visit -> {
                if (!visit.getDate().isBefore(LocalDate.now())) {
                    newVisits.add(visit);
                }
            });
        }
        model.addAttribute("visits", newVisits);
        model.addAttribute("medicalHistory", medicalHistory);
        return "booked_visits";
    }

    @PostMapping("/cancelVisit")
    public String cancelVisit(@RequestParam Long visitId, @RequestParam Long medicalHistoryId,  Model model){
        Visit visit = visitService.findById(visitId).get();
        Meeting meeting = meetingService.findById(visit.getMeeting().getId()).get();
        meeting.setAvailable(true);
        medicalHistoryService.deleteVisit(medicalHistoryId, visit);
        visitService.delete(visit);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(auth.getName());

        Customer customer = customerService.findByUsername(user.getUsername());
        List<Visit> visits = customerService.getVisitsById((customer.getId()));
        MedicalHistory medicalHistory = customer.getMedicalHistory();

        List<Visit> newVisits = new ArrayList<>();
        if(!visits.isEmpty()) {
            visits.forEach(visit2 -> {
                if (!visit2.getDate().isBefore(LocalDate.now())) {
                    newVisits.add(visit2);
                }
            });
        }
        model.addAttribute("visits", newVisits);
        model.addAttribute("medicalHistory", medicalHistory);

        return "booked_visits";
    }


}
