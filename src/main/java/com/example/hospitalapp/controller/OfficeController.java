package com.example.hospitalapp.controller;

import com.example.hospitalapp.persistance.entity.Doctor;
import com.example.hospitalapp.persistance.entity.Office;
import com.example.hospitalapp.service.DoctorService;
import com.example.hospitalapp.service.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class OfficeController {
    private OfficeService officeService;
    private DoctorService doctorService;

    @Autowired
    public OfficeController(OfficeService officeService, DoctorService doctorService) {
        this.officeService = officeService;
        this.doctorService = doctorService;
    }

    @GetMapping("/offices")
    public String getOffices(Model model){
        List<Office> offices = officeService.findAll();
        List<Doctor> doctors = doctorService.findAll();
        model.addAttribute("offices", offices);
        model.addAttribute("doctors", doctors);
        return "offices";
    }


    @GetMapping("/offices/{id}")
    public String getDoctor(@PathVariable("id") Long officeId, Model model){
        Office office = officeService.findById(officeId).get();
        List<Doctor> doctors = doctorService.findAll();
        model.addAttribute("doctors", doctors);
        model.addAttribute("office", office);
        return "office";
    }
}
