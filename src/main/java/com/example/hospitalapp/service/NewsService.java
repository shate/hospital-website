package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.News;
import com.example.hospitalapp.persistance.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class NewsService {

    private NewsRepository newsRepository;

    @Autowired
    public NewsService(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    public Optional<News> findById(Long id){
        return newsRepository.findById(id);
    }

    public News save(News news){
        return newsRepository.save(news);
    }
}
