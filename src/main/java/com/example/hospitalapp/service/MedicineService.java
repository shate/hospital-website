package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.Medicine;
import com.example.hospitalapp.persistance.repository.MedicineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MedicineService {
    private MedicineRepository medicineRepository;

    @Autowired
    public MedicineService(MedicineRepository medicineRepository) {
        this.medicineRepository = medicineRepository;
    }

    public Medicine save(Medicine medicine){
        return medicineRepository.save(medicine);
    }
}
