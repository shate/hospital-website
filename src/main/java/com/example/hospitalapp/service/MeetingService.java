package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.Meeting;
import com.example.hospitalapp.persistance.repository.MeetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MeetingService {

    private MeetingRepository meetingRepository;
    @Autowired
    public MeetingService(MeetingRepository meetingRepository){
        this.meetingRepository = meetingRepository;
    }

    public Meeting save(Meeting meeting){
        return meetingRepository.save(meeting);
    }

    public Optional<Meeting> findById(Long id){
        return meetingRepository.findById(id);
    }
}
