package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.Office;
import com.example.hospitalapp.persistance.entity.Treatment;
import com.example.hospitalapp.persistance.repository.OfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class OfficeService {
    private OfficeRepository officeRepository;

    @Autowired
    public OfficeService(OfficeRepository officeRepository) {
        this.officeRepository = officeRepository;
    }

    public Office save(Office office){
        return officeRepository.save(office);
    }

    public List<Office> findAll(){
        return officeRepository.findAll();
    }

    public Optional<Office> findById(Long id){
        return officeRepository.findById(id);
    }
}
