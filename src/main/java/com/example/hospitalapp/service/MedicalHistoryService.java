package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.Customer;
import com.example.hospitalapp.persistance.entity.MedicalHistory;
import com.example.hospitalapp.persistance.entity.Visit;
import com.example.hospitalapp.persistance.repository.MedicalHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class MedicalHistoryService {

    private MedicalHistoryRepository medicalHistoryRepository;

    @Autowired
    public MedicalHistoryService(MedicalHistoryRepository medicalHistoryRepository) {
        this.medicalHistoryRepository = medicalHistoryRepository;
    }

    public MedicalHistory save(MedicalHistory medicalHistory){
        return medicalHistoryRepository.save(medicalHistory);
    }

    public Optional<MedicalHistory> findById(Long id){
        return medicalHistoryRepository.findById(id);
    }

    public void deleteVisit(Long medicalHistoryId, Visit visit){
        List<Visit> visits = medicalHistoryRepository.findById(medicalHistoryId).get().getVisits();
        visits.remove(visit);
    }
}
