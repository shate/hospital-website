package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.DaySchedule;
import com.example.hospitalapp.persistance.entity.Doctor;
import com.example.hospitalapp.persistance.entity.Treatment;
import com.example.hospitalapp.persistance.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DoctorService {

    private DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public Doctor save(Doctor doctor){
        return doctorRepository.save(doctor);
    }

    public List<Doctor> findAll(){
        return doctorRepository.findAll();
    }

    public List<Doctor> getForTreatment(Long treatmentID){
        List<Doctor> doctors = new ArrayList<>();
        List<Doctor> doctorList = findAll();
        doctorList.forEach(doctor -> {
            List<Treatment> treatmentList = doctor.getTreatments();
            treatmentList.forEach(treatment1 -> {
                if(treatment1.getId().equals(treatmentID) && !doctors.contains(doctor)){
                    doctors.add(doctor);
                }
            });
        });
        return doctors;
    }

    public Optional<Doctor> finByID(Long id){
        return doctorRepository.findById(id);
    }
}
