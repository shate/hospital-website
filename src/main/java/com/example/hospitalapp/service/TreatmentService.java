package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.Office;
import com.example.hospitalapp.persistance.entity.Treatment;
import com.example.hospitalapp.persistance.repository.TreatmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class TreatmentService {
    private TreatmentRepository treatmentRepository;

    @Autowired
    public TreatmentService(TreatmentRepository treatmentRepository) {
        this.treatmentRepository = treatmentRepository;
    }

    public Treatment save(Treatment treatment){
        return treatmentRepository.save(treatment);
    }

    public List<Treatment> findAll(){
        return treatmentRepository.findAll();
    }

    public Optional<Treatment> findById(Long id){
        return treatmentRepository.findById(id);
    }
}
