package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.Visit;
import com.example.hospitalapp.persistance.repository.DoctorRepository;
import com.example.hospitalapp.persistance.repository.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;


@Service
public class VisitService {
    private VisitRepository visitRepository;

    @Autowired
    public VisitService(VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }

    public Visit save(Visit visit){
        return visitRepository.save(visit);
    }

    public Optional<Visit> findById(Long id){
        return visitRepository.findById(id);
    }

    public Iterable<Visit> findAll(){
        return visitRepository.findAll();
    }

    public void delete(Visit visit){
        visitRepository.delete(visit);
    }
}
