package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cglib.core.Local;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

@Component
public class MockData {
    private CustomerService customerService;
    private DiseaseService diseaseService;
    private DoctorService doctorService;
    private MedicalHistoryService medicalHistoryService;
    private MedicineService medicineService;
    private OfficeService officeService;
    private TreatmentService treatmentService;
    private VisitService visitService;
    private DayScheduleService dayScheduleService;
    private MeetingService meetingService;
    private UserService userService;
    private RoleService roleService;
    private ArticleService articleService;
    private NewsService newsService;

    @Autowired
    public MockData(ArticleService articleService, NewsService newsService, RoleService roleService, UserService userService, MeetingService meetingService,
                    DayScheduleService dayScheduleService, CustomerService customerService, DiseaseService diseaseService,
                    DoctorService doctorService, MedicalHistoryService medicalHistoryService, MedicineService medicineService,
                    OfficeService officeService, TreatmentService treatmentService, VisitService visitService) {
        this.articleService = articleService;
        this.newsService = newsService;
        this.roleService = roleService;
        this.userService = userService;
        this.meetingService = meetingService;
        this.dayScheduleService = dayScheduleService;
        this.customerService = customerService;
        this.diseaseService = diseaseService;
        this.doctorService = doctorService;
        this.medicalHistoryService = medicalHistoryService;
        this.medicineService = medicineService;
        this.officeService = officeService;
        this.treatmentService = treatmentService;
        this.visitService = visitService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill(){

        Treatment treatment = new Treatment("Cardiology", "Inserting stents", 150);
        Treatment treatment11 = new Treatment("Cardiology", "ECG", 150);
        Treatment treatment2 = new Treatment("Neurology", "MEG", 200);
        Treatment treatment22 = new Treatment("Neurology", "EEG", 200);
        Treatment treatment3 = new Treatment("Oncology", "Chemotherapy", 300);
        Treatment treatment4 = new Treatment("Rehabilitation", "General movement", 120);
        Treatment treatment5 = new Treatment("Psychology", "Psychologist consultation", 150);
        Treatment treatment6 = new Treatment("Cardiology", "Arteriography", 150);
        List<Treatment> treatments = Arrays.asList(treatment, treatment2, treatment3, treatment4, treatment5, treatment6, treatment11, treatment22);
        Office office = new Office("Cardiology", "Heart problems", Arrays.asList(treatment, treatment11, treatment6));
        Office office2 = new Office("Neurology", "Brain problems", Arrays.asList(treatment2, treatment22));
        Office office3 = new Office("Oncology", "Cancer problems", Arrays.asList(treatment3));
        Office office4 = new Office("Physiology", "Muscle problems", Arrays.asList(treatment4));
        Office office5 = new Office("Psycho", "Psychological problems", Arrays.asList(treatment5));

        List<Office> offices = Arrays.asList(office, office2, office3);
        List<Office> offices2 = Arrays.asList(office2, office4, office5);
        List<Office> offices3 = Arrays.asList(office, office3, office5);
        List<Office> offices4 = Arrays.asList(office, office4, office5);

        Article article2 = new Article("Article about cardiology and neurology", "This is article about cardiology and neurology and this text is its content",
                Arrays.asList(office, office2), LocalDate.of(2020, 5, 18), "Paulo Coelho");

        Article article = new Article("Article about Psychology", "This is article about psychology and this text is its content",
                Arrays.asList(office5), LocalDate.of(2020, 5, 17), "Paulo Coelho");

        News news = new News(Arrays.asList(article, article2));

        Doctor doctor1 = new Doctor("Marek", "Szuwarek", LocalDate.of(1995, 10, 10),
                "marek", "marek", "marek@mail", 15, 7, offices,
                "PUM", "Very good cardiologist", treatments, generateDaySchedules());
        Doctor doctor2 = new Doctor("Ela", "Szela", LocalDate.of(1995, 10, 10),
                "ela", "ela", "ela@mail", 15, 7, offices2,
                "UMED Wroclaw", "Very good neurologist", treatments, generateDaySchedules());
        Doctor doctor3 = new Doctor("Zbigniew", "Lese", LocalDate.of(1995, 10, 10),
                "zbysio", "zbysio", "zbysio@mail", 15, 7, offices3,
                "Umed Warszawa", "Very good psychologist", treatments, generateDaySchedules());
        Doctor doctor4 = new Doctor("Marcin", "Moqgli", LocalDate.of(1995, 10, 10),
                "marcin", "marcin", "marcin@mail", 15, 7, offices4,
                "PUM", "Very good psychologist", treatments, generateDaySchedules());

        Disease disease = new Disease("Parkinson", "Trzesa sie rece");
        Medicine medicine = new Medicine("LEK", "dobry lek");

        List<Disease> diseases = Arrays.asList(disease);
        List<Medicine> medicines = Arrays.asList(medicine);

        List<Visit> visits = new ArrayList<>();
        MedicalHistory medicalHistory = new MedicalHistory(visits, medicines, diseases);

        Customer customer = new Customer("Jan", "Kowalski", LocalDate.of(1990, 12, 12), "jan", "jan", "tomek7289@gmail.com", 80, 180, medicalHistory);


        diseaseService.saveDisease(disease);
        medicineService.save(medicine);
        medicalHistoryService.save(medicalHistory);
        Role role = new Role("ROLE_CUSTOMER");
        roleService.save(role);
        String password = customer.getPassword();
        User customerUser = new User(customer.getUsername(), password, true);
        userService.saveUser(customerUser);
        customerService.saveCustomer(customer);
        treatmentService.save(treatment);
        treatmentService.save(treatment2);
        treatmentService.save(treatment3);
        treatmentService.save(treatment4);
        treatmentService.save(treatment5);
        treatmentService.save(treatment6);
        treatmentService.save(treatment11);
        treatmentService.save(treatment22);
        officeService.save(office);
        officeService.save(office2);
        officeService.save(office3);
        officeService.save(office4);
        officeService.save(office5);
        doctorService.save(doctor1);
        doctorService.save(doctor2);
        doctorService.save(doctor3);
        doctorService.save(doctor4);
        articleService.save(article);
        articleService.save(article2);
        newsService.save(news);



    }

    public List<DaySchedule> generateDaySchedules(){
        DaySchedule daySchedule = new DaySchedule(LocalDate.of(2020, 5, LocalDate.now().getDayOfMonth()), generateMeetings());
        DaySchedule daySchedule2 = new DaySchedule(LocalDate.of(2020, 5, LocalDate.now().getDayOfMonth() + 1), generateMeetings());
        DaySchedule daySchedule3 = new DaySchedule(LocalDate.of(2020, 5, LocalDate.now().getDayOfMonth() + 2), generateMeetings());
        DaySchedule daySchedule4 = new DaySchedule(LocalDate.of(2020, 5, LocalDate.now().getDayOfMonth() + 3), generateMeetings());

        List<DaySchedule> schedules = Arrays.asList(daySchedule, daySchedule2, daySchedule3, daySchedule4);
        dayScheduleService.saveSchedule(daySchedule);
        dayScheduleService.saveSchedule(daySchedule2);
        dayScheduleService.saveSchedule(daySchedule3);
        dayScheduleService.saveSchedule(daySchedule4);
        return schedules;
    }

    public List<Meeting> generateMeetings(){
        Meeting meeting = new Meeting(LocalTime.of(10, 20), LocalTime.of(10, 40), true);
        Meeting meeting2 = new Meeting(LocalTime.of(10, 40), LocalTime.of(11, 0), true);
        Meeting meeting3 = new Meeting(LocalTime.of(11, 0), LocalTime.of(11, 20), true);
        List<Meeting> meetings = Arrays.asList(meeting, meeting2, meeting3);

        meetingService.save(meeting);
        meetingService.save(meeting2);
        meetingService.save(meeting3);
        return meetings;
    }
}
