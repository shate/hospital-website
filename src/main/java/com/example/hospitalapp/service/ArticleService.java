package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.Article;
import com.example.hospitalapp.persistance.entity.News;
import com.example.hospitalapp.persistance.repository.ArticleRepository;
import com.example.hospitalapp.persistance.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ArticleService {

    private ArticleRepository articleRepository;

    @Autowired
    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Optional<Article> findById(Long id){
        return articleRepository.findById(id);
    }

    public Article save(Article article){
        return articleRepository.save(article);
    }
}

