package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.Customer;
import com.example.hospitalapp.persistance.entity.MedicalHistory;
import com.example.hospitalapp.persistance.entity.Visit;
import com.example.hospitalapp.persistance.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    private CustomerRepository customerRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer saveCustomer(Customer customer){
        customer.setPassword(bCryptPasswordEncoder.encode(customer.getPassword()));
        sendEmail(customer);
        return customerRepository.save(customer);
    }

    public Optional<Customer> findById(Long id){
        return customerRepository.findById(id);
    }

    public Customer findByUsername(String username){
        return customerRepository.findByUsername(username);
    }

    public List<Visit> getVisitsById(Long id){
        Customer customer = findById(id).get();
        MedicalHistory medicalHistory =  customer.getMedicalHistory();
        Iterator<Visit> visitIterator = medicalHistory.getVisits().iterator();
        List<Visit> visits = new ArrayList<>();
        visitIterator.forEachRemaining(visits::add);
        return visits;
    }


    void sendEmail(Customer customer) {

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(customer.getEmail());

        msg.setSubject("Welcome on our website!");
        msg.setText(String.format("Hello %s %s, nice to meet you", customer.getFirstName(), customer.getSurname()));

        javaMailSender.send(msg);

    }

    void sendEmailWithAttachment() throws MessagingException, IOException {

        MimeMessage msg = javaMailSender.createMimeMessage();

        // true = multipart message
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);

        helper.setTo("to_@email");

        helper.setSubject("Testing from Spring Boot");

        // default = text/plain
        //helper.setText("Check attachment for image!");

        // true = text/html
        helper.setText("<h1>Check attachment for image!</h1>", true);

        // hard coded a file path
        //FileSystemResource file = new FileSystemResource(new File("path/android.png"));

        helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));

        javaMailSender.send(msg);

    }
}
