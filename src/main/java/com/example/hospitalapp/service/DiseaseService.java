package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.Disease;
import com.example.hospitalapp.persistance.repository.DiseaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiseaseService {

    private DiseaseRepository diseaseRepository;

    @Autowired
    public DiseaseService(DiseaseRepository diseaseRepository) {
        this.diseaseRepository = diseaseRepository;
    }

    public Disease saveDisease(Disease disease){
        return diseaseRepository.save(disease);
    }
}
