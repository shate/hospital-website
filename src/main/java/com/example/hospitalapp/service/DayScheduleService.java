package com.example.hospitalapp.service;

import com.example.hospitalapp.persistance.entity.DaySchedule;
import com.example.hospitalapp.persistance.repository.DayScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DayScheduleService {
    private DayScheduleRepository dayScheduleRepository;

    @Autowired
    public DayScheduleService(DayScheduleRepository dayScheduleRepository){
        this.dayScheduleRepository = dayScheduleRepository;
    }

    public Optional<DaySchedule> findByID(Long id){
        return dayScheduleRepository.findById(id);
    }

    public DaySchedule saveSchedule(DaySchedule daySchedule){
        return dayScheduleRepository.save(daySchedule);
    }

}
